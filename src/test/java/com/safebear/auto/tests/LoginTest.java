package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends Basetests {
    @Test
    public void LoginTest(){

        // Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getUrl());

        Assert.assertEquals(loginPage.getPageTitle(), "Login Page" , "The Login Page didn't open, or the title text has changed");

        //step two ACTION: Enter Username and Password
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");

        //Step three ACTION: Press the Login Button
        loginPage.clickLoginButton();
        //Step three EXPECTED RESULT: checkthat we're now on the the Tools Page
        Assert.assertEquals(toolsPage.getPageTitle(), "Tools Page", "The Login Page didn't open, or te title text has changed");




    }
}
